export interface Adress {
  streetAddress: string;
  city: string;
  state: string;
  zip: string;
}

export interface User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  adress: Adress;
  description: string;
}

export interface InputRows {
  id: string;
  label: string;
  name: string;
  type: string;
  value: number;
}

export interface State {
  users: User[];
  errors: boolean;
  loading: boolean;
  filter: string;
  page: number;
  currentUser: User | null;
  currentPage: number;
  rowsOnPage: number;
  rowsCount: number;
  rowsInputs: InputRows[];
}
