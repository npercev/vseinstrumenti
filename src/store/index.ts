import Vue from "vue";
import Vuex from "vuex";

import { State, User } from "../interfaces";

Vue.use(Vuex);

const state: State = {
  users: [],
  errors: false,
  loading: false,
  filter: "",
  page: 0,
  currentUser: null,
  currentPage: 0,
  rowsOnPage: 10,
  rowsCount: 32,
  rowsInputs: [
    {
      id: "count-small",
      label: "Загрузить малый объём данных",
      name: "count",
      type: "radio",
      value: 32,
    },
    {
      id: "count-big",
      label: "Загрузить большой объём данных",
      name: "count",
      type: "radio",
      value: 1000,
    },
  ],
};

export default new Vuex.Store({
  state,
  mutations: {
    setUsers: (state, users) => {
      state.users = users.map((el: User) => {
        el.id = Number(el.id);
        return el;
      });
    },
    loading: (state, loading) => (state.loading = loading),
    setFilter: (state, filter) => (state.filter = filter),
    setPage: (state, page) => {
      if (
        page > 0 &&
        page <= Math.ceil(state.users.length / state.rowsOnPage)
      ) {
        state.page = page - 1;
      }
    },
    setCurrentUser: (state, user) => (state.currentUser = user),
    setRowsCount: (state, count) => (state.rowsCount = count),
    setRowsOnPage: (state) => {
      state.rowsOnPage = state.rowsCount > 200 ? 50 : 10;
    },
  },
  getters: {
    getUsersList: (state) => {
      if (state.filter) {
        return state.users.filter((user: User) => {
          let result = false;
          result = Object.values(user).some((prop) => {
            if (typeof prop === "string" || typeof prop === "number") {
              return (
                prop
                  .toString()
                  .toLowerCase()
                  .indexOf(state.filter.toLowerCase()) >= 0
              );
            }
            return false;
          });
          return result;
        });
      }
      return state.users;
    },
    getCurrentUser: (state) => state.currentUser,
    getCurrentPage: (state) => state.page,
    getPageCount: (state) => Math.ceil(state.users.length / state.rowsOnPage),
    getURL: (state) => {
      return `http://www.filltext.com/?rows=${state.rowsCount}&id=%7Bnumber%7C1000%7D&firstName=%7BfirstName%7D&lastName=%7BlastName%7D&email=%7Bemail%7D&phone=%7Bphone%7C(xxx)xxx-xx-xx%7D&adress=%7BaddressObject%7D&description=%7Blorem%7C32%7D`;
    },
    getCountInput: (state) => state.rowsInputs,
  },
  actions: {
    async fetchUsers({ commit, getters }) {
      try {
        commit("loading", true);
        commit("setPage", 1);
        const response = await fetch(getters.getURL);
        const result = await response.json();
        commit("setUsers", result);
      } catch (e) {
        console.log("error", e);
      } finally {
        commit("setRowsOnPage");
        commit("loading", false);
      }
    },
  },
  modules: {},
});
